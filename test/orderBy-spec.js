var path = require('path');
var assert = require('assert');

describe('OrderBy', function () {

    it('should exist', function () {
        var orderByFilePath = path.resolve('./helpers/orderBy');
        var orderBy = require(orderByFilePath);
        assert.notEqual(orderBy, null);
    });

    it('should sort the list by \'name\' property', function(){

        var orderByFilePath = path.resolve('./helpers/orderBy');
        var orderBy = require(orderByFilePath);
        var list = [{name:'cab'},{name:'bca'},{name:'abc'}];

        var sortedList = orderBy(list,'name');

        assert.equal('abc', sortedList[0].name);
        assert.equal('bca', sortedList[1].name);
        assert.equal('cab', sortedList[2].name);

    });

});
