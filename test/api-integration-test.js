var chai = require('chai');
chai.should();
var chaiHttp = require('chai-http');
var path = require('path')
var app = path.resolve('./app.js');

var server = require(app);

chai.use(chaiHttp);

describe("API-integration-test", function () {
    it("/api/v1/categories/list/flatten", function (done) {
        var categories = [
            {"category": "Family", "parent": null},
            {"category": "Parenting", "parent": "Family"},
            {"category": "Caregiving", "parent": "Family"},
            {"category": "Finances", "parent": "Family"},
            {"category": "RRSPs", "parent": "Finances"},
            {"category": "Savings", "parent": "Finances"},
            {"category": "Planning", "parent": "Finances"},
            {"category": "Career", "parent": null},
            {"category": "Managers", "parent": "Career"},
            {"category": "Employees", "parent": "Career"}
        ];
        chai.request(server)
            .post('/api/v1/categories/list/flatten')
            .send(categories)
            .end(function (error, response) {

                response.should.have.status(200);
                response.body[0].should.equal('Career');
                response.body[1].should.equal('Employees');
                response.body[2].should.equal('Managers');
                response.body[3].should.equal('Family');
                response.body[4].should.equal('Caregiving');
                response.body[5].should.equal('Finances');
                response.body[6].should.equal('Planning');
                response.body[7].should.equal('RRSPs');
                response.body[8].should.equal('Savings');
                response.body[9].should.equal('Parenting');

                done();

            });
    });
    it("/api/v1/categories/tree/flatten", function (done) {
        var categories = [
            {
                "category": "Family",
                "subCategories": [
                    {"category": "Parenting"},
                    {"category": "Caregiving"},
                    {
                        "category": "Finances",
                        "subCategories": [{"category": "RRSPs"}, {"category": "Planning"}, {"category": "Savings"}]
                    }

                ]
            },
            {"category": "Career", "subCategories": [{"category": "Managers"}, {"category": "Employees"}]}

        ];

        chai.request(server)
            .post('/api/v1/categories/tree/flatten')
            .send(categories)
            .end(function (error, response) {

                response.should.have.status(200);
                response.body[0].should.equal('Career');
                response.body[1].should.equal('Employees');
                response.body[2].should.equal('Managers');
                response.body[3].should.equal('Family');
                response.body[4].should.equal('Caregiving');
                response.body[5].should.equal('Finances');
                response.body[6].should.equal('Planning');
                response.body[7].should.equal('RRSPs');
                response.body[8].should.equal('Savings');
                response.body[9].should.equal('Parenting');

                done();

            });
    });
});


