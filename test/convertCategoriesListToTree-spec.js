var path = require('path');
var assert = require('assert');

describe('convertCategoriesListToTree', function () {

    it('should exist', function () {
        var convertCategoriesListToTreeFilePath = path.resolve('./helpers/convertCategoriesListToTree.js');
        var convertCategoriesListToTree = require(convertCategoriesListToTreeFilePath);
        assert.notEqual(convertCategoriesListToTree, null);
    });

    it('should convert the category list to a category tree', function(){

        var convertCategoriesListToTreeFilePath = path.resolve('./helpers/convertCategoriesListToTree.js');
        var convertCategoriesListToTree = require(convertCategoriesListToTreeFilePath);

        var categories = [
            {category: "Family", parent: null},
            {category: "Finances", parent: "Family"},
            {category: "RRSPs", parent: "Finances"},
            {category: "Career", parent: null},
            {category: "Managers", parent: "Career"}

        ];
        var categoriesTree = convertCategoriesListToTree(categories);

        assert.equal('Family', categoriesTree[0].category);
        assert.equal('Finances', categoriesTree[0].subCategories[0].category);
        assert.equal('RRSPs',  categoriesTree[0].subCategories[0].subCategories[0].category);
        assert.equal('Career', categoriesTree[1].category);
        assert.equal('Managers', categoriesTree[1].subCategories[0].category);

    });

});
