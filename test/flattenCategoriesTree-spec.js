var path = require('path');
var assert = require('assert');

describe('flattenCategoriesTree', function () {

    it('should exist', function () {
        var flattenCategoriesTreeFilePath = path.resolve('./helpers/flattenCategoriesTree.js');
        var flattenCategoriesTree = require(flattenCategoriesTreeFilePath);
        assert.notEqual(flattenCategoriesTree, null);
    });

    it('should convert the category tree to a category flat list', function () {

        var flattenCategoriesTreeFilePath = path.resolve('./helpers/flattenCategoriesTree.js');
        var flattenCategoriesTree = require(flattenCategoriesTreeFilePath);

        var categories = [
            {
                category: 'Family',
                subCategories: [
                    {category: 'Parenting'},
                    {category: 'Caregiving'},
                    {
                        category: 'Finances',
                        subCategories: [{category: 'RRSPs'}, {category: 'Planning'}, {category: 'Savings'}]
                    },

                ]
            },
            {category: 'Career', subCategories: [{category: 'Managers'}, {category: 'Employees'}]}

        ]

        var categoriesFlatList = flattenCategoriesTree(categories);

        assert.equal("Career", categoriesFlatList[0]);
        assert.equal("Employees", categoriesFlatList[1]);
        assert.equal("Managers", categoriesFlatList[2]);
        assert.equal("Family", categoriesFlatList[3]);
        assert.equal("Caregiving", categoriesFlatList[4]);
        assert.equal("Finances", categoriesFlatList[5]);
        assert.equal("Planning", categoriesFlatList[6]);
        assert.equal("RRSPs", categoriesFlatList[7]);
        assert.equal("Savings", categoriesFlatList[8]);
        assert.equal("Parenting", categoriesFlatList[9]);

    });

});
