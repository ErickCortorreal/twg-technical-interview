module.exports =function convertCategoriesListToTree(categories) {

    var categoriesList = categories.slice();
    var categoriesMap = categoriesList.reduce(function (map, category) {
        map[category.category] = category;
        return map;
    }, {});


    var categoriesTree = [];
    categoriesList.forEach(function (category) {

        var parent = categoriesMap[category.parent];
        if (parent) {
            var subCategories = (parent.subCategories || (parent.subCategories = []));

            subCategories.push(category);
        } else {

            categoriesTree.push(category);
        }
    });

    return categoriesTree;

};