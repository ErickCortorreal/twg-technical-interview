var path = require('path')
var orderByFilePath = path.resolve('./helpers/orderBy.js');
var orderBy = require(orderByFilePath);

module.exports = function flattenCategoriesTree(categoriesTree) {


    var categoriesFlatList = [];

    function flatify(categoriesTree) {
        categoriesTree = orderBy(categoriesTree, 'category');
        for (var i = 0; i < categoriesTree.length; i++) {
            var currentCategory = categoriesTree[i];
            categoriesFlatList.push(currentCategory.category);
            if (currentCategory.subCategories) {
                flatify(currentCategory.subCategories);
            }

        }
    }

    flatify(categoriesTree);

    return categoriesFlatList;
}
