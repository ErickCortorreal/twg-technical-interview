module.exports = function orderBy(list,property){
    var listToSort = list.slice();
    function compare(a, b) {
        if (a[property] < b[property])
            return -1;
        if (a[property]> b[property])
            return 1;
        return 0;
    }
    return listToSort.sort(compare);

}