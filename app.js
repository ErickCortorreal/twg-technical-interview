var express = require('express');
var path = require('path');
var bodyParser = require('body-parser')

var flattenCategoriesTreeFilePath = path.resolve('./helpers/flattenCategoriesTree.js');
var flattenCategoriesTree = require(flattenCategoriesTreeFilePath);

var convertCategoriesListToTreeFilePath = path.resolve('./helpers/convertCategoriesListToTree.js');
var convertCategoriesListToTree = require(convertCategoriesListToTreeFilePath);

var app = express();
app.use(bodyParser.json())

app.post('/api/v1/categories/list/flatten', function (req, res) {
    var categoriesList = req.body;

    var categoriesTree = convertCategoriesListToTree(categoriesList);

    var flatCategoriesList = flattenCategoriesTree(categoriesTree);

    res.send(flatCategoriesList);

});

app.post('/api/v1/categories/tree/flatten', function (req, res) {

    var categoriesTree = req.body;

    var flatCategoriesList = flattenCategoriesTree(categoriesTree);

    res.send(flatCategoriesList);
});

app.listen(3000);

console.log("App running at localhost:3000");

module.exports = app;